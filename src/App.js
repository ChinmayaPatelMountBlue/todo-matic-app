import './App.css';
import Todo from './components/todo/Todo';
import Form from './components/form/Form';
import Buttons from './components/buttons/Buttons';
import { nanoid } from 'nanoid';
import React, { useState, useRef, useEffect } from 'react';

function App() {

  const headingRef = useRef(null);

  const [displayTasks, setDisplayTasks] = useState("all");

  const initialTasks = [
    {
      "id": "todo-0",
      "name": "Eat",
      "completed": true
    },
    {
      "id": "todo-1",
      "name": "Sleep",
      "completed": false
    },
    {
      "id": "todo-2",
      "name": "Repeat",
      "completed": false
    }
  ];

  const [tasks, setTasks] = useState(initialTasks);

  function usePrevious(value){

    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    })

    return ref.current;

  }

  function toggleDisplay(condition){

    setDisplayTasks(condition);

  }

  function deleteTask(id){

    const updatedTasks = tasks.filter((task) => {

      return task.id !== id;

    });

    console.log(updatedTasks);

    setTasks(updatedTasks);

  }

  function toggleCompleteAction(id){

    const updatedTasks = tasks.map((currentTask) => {

      if(currentTask.id === id){

        return {...currentTask, "completed": !currentTask.completed};

      }

      return currentTask;

    });

    setTasks(updatedTasks);

  }

  const editTask = (id, newName) => {

    const updatedTasks = tasks.map((task) => {

      if(task.id === id){
        return {...task, "name": newName};
      }

      return task;

    })

    setTasks(updatedTasks);

  }

  let taskList = tasks?.map((task) => {

    const taskId = task.id ? task.id : "todo-" + nanoid();

    return <Todo
      id={taskId}
      name={task.name}
      completed={task.completed}
      key={taskId}
      toggleCompleteAction={toggleCompleteAction}
      deleteTask={deleteTask}
      editTask={editTask}
    />

  });

  function addTask(name){
    const newTask = {
      "id": "todo"+nanoid(),
      "name": name,
      "completed": false
    };

    setTasks([...tasks, newTask]);
  }

  const allTasks = taskList;

  const activeTasks = tasks.filter((task) => {

    return task.completed === false;

  }).map((task) => {

    return <Todo
      id={task.id}
      name={task.name}
      completed={task.completed}
      key={task.id}
      toggleCompleteAction={toggleCompleteAction}
      deleteTask={deleteTask}
      editTask={editTask}
    />

  });

  const completedTasks = tasks.filter((task) => {

    return task.completed;

  }).map((task) => {

    return <Todo
      id={task.id}
      name={task.name}
      completed={task.completed}
      key={task.id}
      toggleCompleteAction={toggleCompleteAction}
      deleteTask={deleteTask}
      editTask={editTask}
    />

  });

  let tasksList;

  if(displayTasks === "all"){
    tasksList = allTasks;
  }
  else if(displayTasks === "active"){
    tasksList = activeTasks;
  }
  else{
    tasksList = completedTasks;
  }

  const prevTaskLength = usePrevious(tasksList.length);

  useEffect(() => {

    if(tasksList.length - prevTaskLength === -1){

      headingRef.current.focus();

    }

  }, [tasksList.length, prevTaskLength]);

  const headingNoun = tasksList.length === 1 ? "task" : "tasks" ;
  const headingText =  `${tasksList.length} ${headingNoun} remaining`;

  return (
    <div className="outer-container">

      <h1>What needs to be done?</h1>

      <Form
        addTask={addTask} 
      />

      <Buttons 
        toggleDisplay={toggleDisplay}
      />

      <div id="tasksRemaining" className="tasks-remaining" ref={headingRef} tabIndex="-1">{headingText}</div>

      <div id="taskContainer" className="task-container">

        <ul id="taskList" className="task-list">

          {tasksList}          

        </ul>

      </div>

    </div>
  );
}

export default App;