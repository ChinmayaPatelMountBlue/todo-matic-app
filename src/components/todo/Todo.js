import React,{ useState, useRef, useEffect } from 'react';

export default function Todo(props){

    const editFieldRef = useRef(null);

    const editButtonRef = useRef(null);

    const [isEditing, setIsEditing] = useState(false);

    const wasEditing = usePrevious(isEditing);

    function usePrevious(value){

        const ref = useRef();

        useEffect(() => {

            ref.current = value;

        })

        return ref.current;


    }

    useEffect(() => {

        if(!wasEditing && isEditing){

            editFieldRef.current.focus();

        }
        if(wasEditing && !isEditing){

            editButtonRef.current.focus();

        }

    }, [isEditing, wasEditing]);

    const [newName, setNewName] = useState(props.name);

    function displayError(){
        
        const errBlock = document.querySelector("#errMsg");

        errBlock.classList.add("visible");

        console.log(errBlock.classList);

        setTimeout(() => {

            errBlock.classList.remove("visible");

        }, 1890);

    }

    function handleChange(e){
        setNewName(e.target.value);
    }

    function handleSubmit(e){
        e.preventDefault();
        if(newName===""){
            displayError();
        }
        else{
            props.editTask(props.id, newName);
        }
        setNewName(newName);
        setIsEditing(false);
    }

    const viewTemplate = (

        <>
            <input
                type="checkbox"
                defaultChecked={props.completed}
                onChange={() => props.toggleCompleteAction(props.id)}
            />
            {props.completed ? <span className="todo-task strike-through">{props.name}</span> : <span className="todo-task">{props.name}</span>}
            <div className="list-btn-container">
                <button
                    className="btn edit-btn"
                    onClick={()=>setIsEditing(true)}
                    ref={editButtonRef}
                >EDIT</button>
                <button
                    className="btn delete-btn"
                    onClick={() => props.deleteTask(props.id)}
                >DELETE</button>
            </div>
        </>

    );

    const editTemplate = (

        <>

            <form className="form-group-edit" onSubmit={handleSubmit}>

                <label htmlFor="editTaskInput"></label>
                <input
                    type="text"
                    className="task-input"
                    id="editTaskInput"
                    value={newName}
                    onChange={handleChange}
                    ref={editFieldRef}
                />
                <div className="list-btn-container">
                    <button type="button" onClick={()=>setIsEditing(false)} className="btn">CANCEL</button>
                    <button type="submit" className="btn btn-submit">SUBMIT</button>
                </div>

            </form>

        </>

    );

    return(

        <li className="task" id={props.id}>
            {isEditing ? editTemplate : viewTemplate}
        </li>

    );

}