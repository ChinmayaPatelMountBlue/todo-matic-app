import React from 'react';

export default function Buttons(props){

    return(

        <div className="btn-container">

        <ul className="btn-list">

          <li>
            <button id="btnAll" className="btn" onClick={() => props.toggleDisplay("all")}>
              <span className="visibly-hidden">Show</span>
              <span>All</span>
              <span className="visibly-hidden">Tasks</span>
            </button>
          </li>
          <li>
            <button id="btnActive" className="btn" onClick={() => props.toggleDisplay("active")}>
            <span className="visibly-hidden">Show</span>
              <span>Active</span>
              <span className="visibly-hidden">Tasks</span>
            </button>
          </li>
          <li>
            <button id="btnCompleted" className="btn" onClick={() => props.toggleDisplay("completed")}>
            <span className="visibly-hidden">Show</span>
              <span>Completed</span>
              <span className="visibly-hidden">Tasks</span>
            </button>
          </li>

        </ul>

      </div>

    );

}