import React, { useState } from 'react';

export default function Form(props){

    const [name, setName] = useState("");

    function displayError(){
        
        const errBlock = document.querySelector("#errMsg");

        errBlock.classList.add("visible");

        console.log(errBlock.classList);

        setTimeout(() => {

            errBlock.classList.remove("visible");

        }, 1890);

    }

    function handleChange(e) {
        setName(e.target.value);
    }

    function handleSubmit(e){
        e.preventDefault();
        if(name === ""){
            displayError();
        }
        else{
            props.addTask(name);
        }
        setName("");
    }

    return(

        <form className="form-group" onSubmit={handleSubmit}>

            <label htmlFor="taskInput"></label>
            <input
                type="text"
                id="taskInput"
                className="task-input"
                name="taskInput"
                placeholder="Add Task here"
                value={name}
                onChange={handleChange}
            />
            <button id="btnAdd" className="btn">ADD</button>
            <div id="errMsg" className="err-msg">Task Empty!</div>

        </form>

    );

}